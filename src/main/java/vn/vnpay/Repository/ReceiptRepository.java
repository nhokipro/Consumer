package vn.vnpay.Repository;

import lombok.SneakyThrows;
import vn.vnpay.ConnectDB.ConnectionPool;
import vn.vnpay.Entity.Receipt;

import java.sql.*;

public class ReceiptRepository {

    ConnectionPool connectionPool;

    public ReceiptRepository() {
        connectionPool = new ConnectionPool();
    }

    Connection connection = null;

    @SneakyThrows
    public Receipt findById(Long id) {
        Connection connection = connectionPool.getConnection();

        String sql = "SELECT * FROM Receipt " +
                "WHERE id = ?";
        PreparedStatement preparedStatement = connection.prepareStatement(sql);
        preparedStatement.setLong(1, id);
        ResultSet resultSet = preparedStatement.executeQuery();
        if (resultSet.next()) {
            Receipt receipt = Receipt.builder()
                    .id(resultSet.getLong(1))
                    .tokenKey(resultSet.getString(2))
                    .apiID(resultSet.getString(3))
                    .mobile(resultSet.getString(4))
                    .bankCode(resultSet.getString(5))
                    .accountNo(resultSet.getString(6))
                    .payDate(resultSet.getDate(7))
                    .additionalData(resultSet.getString(8))
                    .debitAmount(resultSet.getDouble(9))
                    .respCode(resultSet.getString(10))
                    .respDesc(resultSet.getString(11))
                    .traceTransfer(resultSet.getString(12))
                    .messageType(resultSet.getString(13))
                    .checkSum(resultSet.getString(14))
                    .orderCode(resultSet.getString(15))
                    .username(resultSet.getString(16))
                    .realAmount(resultSet.getDouble(17))
                    .promotionCode(resultSet.getString(18))
                    .createDate(resultSet.getDate(19))
                    .build();
            return receipt;
        }
        return null;
    }

    public Receipt createReceipt(Receipt receipt) throws SQLException {
        Connection connection = connectionPool.getConnection();

        String sql = "INSERT INTO `Receipt` (token_key, api_id, mobile, bank_code, accountNo, additional_data, debit_amount," +
                " respCode, respDesc, trace_transfer, message_type, check_sum, order_code, user_name, real_amount, promotion_code) "
                + "VALUE( ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        PreparedStatement preparedStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

        preparedStatement.setString(1, receipt.getTokenKey());
        preparedStatement.setString(2, receipt.getApiID());
        preparedStatement.setString(3, receipt.getMobile());
        preparedStatement.setString(4, receipt.getBankCode());
        preparedStatement.setString(5, receipt.getAccountNo());
        preparedStatement.setString(6, receipt.getAdditionalData());
        preparedStatement.setDouble(7, receipt.getDebitAmount());
        preparedStatement.setString(8, receipt.getRespCode());
        preparedStatement.setString(9, receipt.getRespDesc());
        preparedStatement.setString(10, receipt.getTraceTransfer());
        preparedStatement.setString(11, receipt.getMessageType());
        preparedStatement.setString(12, receipt.getCheckSum());
        preparedStatement.setString(13, receipt.getOrderCode());
        preparedStatement.setString(14, receipt.getUsername());
        preparedStatement.setDouble(15, receipt.getRealAmount());
        preparedStatement.setString(16, receipt.getPromotionCode());
        preparedStatement.executeUpdate();
        ResultSet rs = preparedStatement.getGeneratedKeys();
        Long generatedKey = 0L;
        if (rs.next()) {
            generatedKey = rs.getLong(1);
        }
        return findById(generatedKey);
    }

    public Boolean existByTokenAndCreateDate(String token, Date createDate) {
        try {
            connection = connectionPool.getConnection();

            String sql = "SELECT 1 FROM `Receipt` WHERE `token_key` = ? AND create_date = ?";
            PreparedStatement preparedStatement = connection.prepareStatement(sql);

            preparedStatement.setString(1, token);
            preparedStatement.setDate(2, createDate);

            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                return true;
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return false;
    }

}