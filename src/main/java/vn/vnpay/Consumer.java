package vn.vnpay;

import com.google.gson.Gson;
import com.rabbitmq.client.*;
import vn.vnpay.Entity.Receipt;
import vn.vnpay.Repository.ReceiptRepository;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.sql.SQLException;
import java.util.concurrent.TimeoutException;

public class Consumer implements Runnable {
    public static final String QUEUE_NAME = "queue";
    private static final ReceiptRepository receiptRepository = new ReceiptRepository();
    private static final Gson gson = new Gson();
    private String name;

    @Override
    public void run() {
        try {
            ConnectionFactory factory = new ConnectionFactory();
            factory.setHost("localhost");
            Connection connection = factory.newConnection();
            Channel channel = connection.createChannel();
            channel.queueDeclare(QUEUE_NAME, false, false, false, null);
            channel.basicQos(1);
            DeliverCallback deliverCallback = (consumerTag, delivery) -> {
                String message = new String(delivery.getBody(), StandardCharsets.UTF_8);
                consume(gson.fromJson(message, Receipt.class));
                channel.basicAck(delivery.getEnvelope().getDeliveryTag(), false);
            };
            CancelCallback cancelCallback = consumerTag -> {
            };
            boolean autoAck = false;
            channel.basicConsume(QUEUE_NAME, autoAck, deliverCallback, cancelCallback);
        } catch (IOException | TimeoutException e) {
            e.printStackTrace();
        }
    }

    private static void consume(Receipt receipt) {
        try {
            receiptRepository.createReceipt(receipt);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
}
